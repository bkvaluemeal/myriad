# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## 0.1.2 - SSL directory
### Changed
- Joined certificates are now written to `$WORKING_DIR/ssl/`

## 0.1.1 - Bug fixes
### Added
- Option to force renew certificates
### Removed
- Shorthand parameter to enable staging
- domains.txt in working directory which conflicted with HaProxy

## 0.1.0 - Digitalocean
### Added
- Support for Digitalocean
