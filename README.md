Myriad
======

A Docker container to automatically obtain Letsencrypt SSL certificates

Install
-------

	docker pull bkvaluemeal/myriad

Usage
-----

	$ myriad --help
	Usage: /usr/bin/myriad [-h] [command [argument]] [parameter [argument]] [parameter [argument]] ...

	Default command: help

	Commands:
	 daemon                                       Start a daemon server
	 run                                          Run once and exit
	 --version (-v)                               Print version information
	 --help (-h)                                  Show help text

	Parameters:
	 --contact-email (-ce) someone@somewhere.com  An email to receive updates about certificate expiration reminders
	 --domain (-d) somewhere.com                  A domain to obtain a certificate for
	 --provider (-p)                              Which provider to use to complete the challenge
	 --staging                                    Use the Let's Encrypt staging server for testing
	 --working-dir (-wd) /tmp/letsencrypt         The working directory to write certificates
	 --algo (-a) rsa|prime256v1|secp384r1         Which public key algorithm should be used? Supported: rsa, prime256v1 and secp384r1
	 --force                                      Force renew of certificate even if it is longer valid than value in RENEW_DAYS

	Providers:
	 digitalocean

	Environment Varibles:
	 DIGITALOCEAN_TOKEN                           The API token to communicate with Digitalocean

License
-------

	Copyright (c) 2017, Justin Willis

	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
